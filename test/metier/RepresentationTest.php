<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Representation Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Representation;
        use modele\metier\Groupe;
        use modele\metier\Lieu;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Représentation</h2>";
        $groupe = new Groupe("g999","les Joyeux Turlurons","général Alcazar","Tapiocapolis" ,25,"San Theodoros","N");
        $lieu = new Lieu('L55', 'La Joliverie', '141 route de Clisson', 25);
        $objet = new Representation("R61","2020-05-23","20:00:00","22:00:00",$groupe,$lieu);
        var_dump($objet);
        ?>
    </body>
</html>
