<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CtrlRepresentations
 *
 * @author racin
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\GroupeDAO;
use modele\dao\LieuDAO;
use modele\dao\RepresentationDAO;
use modele\metier\Groupe;
use modele\metier\Lieu;
use modele\metier\Representation;
use modele\dao\Bdd;
use vue\representations\VueConsultationRepresentations;
use vue\representations\VueSaisieRepresentations;
use vue\representations\VueSupprimerRepresentations;

class CtrlRepresentations extends ControleurGenerique {

    /** controleur= representations & action= defaut
     * Afficher la liste des représentations      */
    public function defaut() {
        $this->consulter();
    }

    /** controleur= representations & action= consulter
     * Afficher la liste des représentations       */
    public function consulter() {
        $laVue = new VueConsultationRepresentations();
        $this->vue = $laVue;
        Bdd::connecter();
        $laVue->setLesGroupes(GroupeDAO::getAll());
        $laVue->setLesLieux(LieuDAO::getAll());
        $laVue->setLesRepresentations(RepresentationDAO::getAll());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - représentations");
        $this->vue->afficher();
    }
    
    /** controleur= représentations & action=creer
     * Afficher le formulaire d'ajout d'une représentation     */
    public function creer() {
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvelle représentation");
        $unGroupe = new Groupe("", "", "", "", "", "", "");
        $unLieu = new Lieu("", "", "", "");
        $uneRepresentation = new Representation("", "", "", "", $unGroupe, $unLieu);
        $laVue->setUneRepresentation($uneRepresentation);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - représentation");
        $this->vue->afficher();
    }

    /** controleur= représentations & action=validerCreer
     * ajouter une représentation dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Representation $uneRepresentation  : récupération du contenu du formulaire et instanciation d'une representation */
        $uneRepresentation = new Representation($_REQUEST['id'], $_REQUEST['date'], $_REQUEST['heuredebut'], $_REQUEST['heurefin'], GroupeDAO::getOneById($_REQUEST['groupe']), LieuDAO::getOneById($_REQUEST['lieu']));        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesRepresentation($uneRepresentation, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer la representation
            RepresentationDAO::insert($uneRepresentation);
            // revenir à  la liste des representation
            header("Location: index.php?controleur=representations&action=consulter");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieRepresentations();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouvelle Représentation");
            $laVue->setUneRepresentation($uneRepresentation);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - représentation");
            $this->vue->afficher();
        }
    }

    /** controleur= representations & action= modifier & id = identifiant de la representation visé */
    public function modifier() {
        $idRepresentation = $_GET["id"];
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;      
        Bdd::connecter();
        $laRepresentation = RepresentationDAO::getOneById($idRepresentation);
        $this->vue->setUneRepresentation($laRepresentation);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la représentation : " . $laRepresentation->getId());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - représentations");
        $this->vue->afficher();
    }
    
    /** controleur= representations & action=validerModifier
     * modifier une representation dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Representation $uneRepresentation  : récupération du contenu du formulaire et instanciation d'une representation */
        $uneRepresentation = new Representation($_REQUEST['id'], $_REQUEST['date'], $_REQUEST['heuredebut'], $_REQUEST['heurefin'], GroupeDAO::getOneById($_REQUEST['groupe']), LieuDAO::getOneById($_REQUEST['lieu']));

        // vÃ©rifier la saisie des champs obligatoires et les contraintes d'intÃ©gritÃ© du contenu
        // pour un formulaire de modification (paramétre n°1 = false)
        $this->verifierDonneesRepresentation($uneRepresentation, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour la representation
            RepresentationDAO::update($uneRepresentation->getId(), $uneRepresentation->getDate(), $uneRepresentation->getHeureDebut(), $uneRepresentation->getHeureFin(), 
                    $uneRepresentation->getUnGroupe(), $uneRepresentation->getUnLieu());
            // revenir Ã  la liste des representations
            header("Location: index.php?controleur=representations&action=consulter");
        } else {
            // s'il y a des erreurs, 
            // rÃ©afficher le formulaire de modification
            $laVue = new VueSaisieRepresentation();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRepresentation);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier la representation : " . $uneRepresentation->getGroupe()->getNom() . " (" . $uneRepresentation->getId() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - representation");
            $this->vue->afficher();
        }
    }
    
    /** controleur= representations & action=supprimer & id=identifiant_representation
     * Supprimer une representation d'aprÃ¨s son identifiant     */
    public function supprimer() {
        $idRepresentation = $_GET["id"];
        $this->vue = new VueSupprimerRepresentations();
        // Lire dans la BDD les données de la representation Ã  supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idRepresentation));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }
    
    /** controleur= representations & action= validerSupprimer
     * supprimer une representation dans la base de données aprés confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de la representation à  supprimer");
        } else {
            // suppression d'une representation d'après son identifiant
            RepresentationDAO::delete($_GET["id"]);
        }
        // retour à  la liste des representations
        header("Location: index.php?controleur=representations&action=consulter");
    }
    
    /**
     * Vérification des données du formulaire de saisie
     * @param Representation $uneRepresentation representation à  vérifier
     * @param bool $creation : =true si formulaire de crÃ©ation d'une nouvelle representation; =false sinon
     */
    private function verifierDonneesRepresentation(Representation $uneRepresentation, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $uneRepresentation->getId() == "") || $uneRepresentation->getDate() == "" || $uneRepresentation->getUnLieu()->getId() == "" || $uneRepresentation->getUnGroupe()->getId() == "" ||
                $uneRepresentation->getHeureDebut() == "" || $uneRepresentation->getHeurefin() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $uneRepresentation->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($uneRepresentation->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (RepresentationDAO::isAnExistingId($uneRepresentation->getId())) {
                    GestionErreurs::ajouter("La Representation " . $uneRepresentation->getId() . " existe déjà ");
                }
            }
        }
    }
    
}
