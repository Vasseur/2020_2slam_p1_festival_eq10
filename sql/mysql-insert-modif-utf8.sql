/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  racin
 * Created: 6 oct. 2020
 */

-- Les lieux sont fictifs
insert into Lieu values ('L01', 'Scène Carle Aubert','9 allée des champignons',60);
insert into Lieu values ('L02', 'Scène Yves-Gérard Bony','12 rue des papillones',90);
insert into Lieu values ('L03', 'Scène Dieudonnée Murielle','1 avenue Saint-Pierre',75);
insert into Lieu values ('L04', 'Scène Séverin Pigeon','18 boulevard des aigles',50);
insert into Lieu values ('L05', 'Scène Bernard-Damien Suarez','3 impasse des petits cailloux',100);

-- Les représentations sont fictives
insert into Representation values ('R01', '2020-05-19','20:00:00','23:00:00','g012','L03');
insert into Representation values ('R02', '2020-05-20','21:00:00','22:00:00','g039','L01');
insert into Representation values ('R03', '2020-05-21','21:30:00','22:30:00','g021','L04');

